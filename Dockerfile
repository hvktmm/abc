FROM harbor.nashteam.io/stepone/base-php-nginx:7.4-alpine

ENV WORK_DIR /var/www/html
ENV PUBLIC_DIR ${WORK_DIR}/public
ENV STORAGE_DIR ${WORK_DIR}/storage
ENV STORAGE_LOG_DIR ${STORAGE_DIR}/logs

WORKDIR ${WORK_DIR}

COPY ["./", "./"]

# permission
RUN chmod -R 777 ${PUBLIC_DIR} ${PUBLIC_DIR}/ ${STORAGE_DIR} ${STORAGE_DIR}/

# Install packages
RUN composer update

# install bootstrap & create auth layout
RUN php artisan ui bootstrap --auth

# publish layout
RUN php artisan infyom.publish:layout

# publish vendor
RUN php artisan vendor:publish --all
RUN php artisan infyom.publish:generator-builder
RUN php artisan jwt:secret


# Install node_modules
RUN npm install

# run webpack build frontend
RUN npm run dev


# generate key for app
RUN php artisan key:generate
# clear config of .env
RUN php artisan config:clear
# clear cache
RUN php artisan cache:clear
EXPOSE 80 443
