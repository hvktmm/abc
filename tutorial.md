## Install packages
Run command line:
```bash
composer install
```

## Enable JWT Authentication
> 1. Publish the package config file. Run command line:

```bash
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
```
or
```bash
php artisan vendor:publish --all
```
> 2. Generate secret key. Run command line:
```bash
php artisan jwt:secret
```

## Enable Auth UI
1. Run command line:

> Only Bootstrap
```bash
php artisan ui bootstrap
```

> For Bootstrap and  auth scaffoldings
```bash
php artisan ui bootstrap --auth
```

Finally, run command ```npm install & npm run dev```

